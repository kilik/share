<?php

namespace App\Manager;

use App\Entity\Message;
use Doctrine\Persistence\ManagerRegistry;
use MongoDB\Driver\Manager;
use Symfony\Bridge\Doctrine\RegistryInterface;

class MessageManager
{
    private ManagerRegistry $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    public function saveMessage(Message $message, int $delay, string $delayType)
    {
        $multiType = [
            'minute' => 60,
            'hour' => 60 * 60,
            'day' => 24 * 60 * 60,
        ];

        if (!isset($multiType[$delayType])) {
            $delayType = 'minute';
        }

        $seconds = $delay * $multiType[$delayType];
        $message->setExpiredAt(new \DateTime(sprintf('+%d seconds', $seconds)));
        $this->registry->getManager()->persist($message);
        $this->registry->getManager()->flush();
    }

    /**
     * Read this message (down remaining views, and delete at the countdown end)
     */
    public function readMessage(Message $message): ?Message
    {
        if ($message->getExpiredAt()->getTimestamp() < time()) {
            $this->registry->getManager()->remove($message);
            $this->registry->getManager()->flush();

            return null;
        } else {
            $message->decRemainingViews();
            if ($message->getRemainingViews() <= 0) {
                $this->registry->getManager()->remove($message);
            }

            $this->registry->getManager()->flush();

            return $message;
        }
    }

}