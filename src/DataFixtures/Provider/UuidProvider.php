<?php

namespace App\DataFixtures\Provider;

use Ramsey\Uuid\Uuid;

class UuidProvider
{
    /**
     * @param string $uuidStr
     * @return \Ramsey\Uuid\UuidInterface
     */
    function RamseyUuid(string $uuidStr)
    {
        return Uuid::fromString($uuidStr);
    }

}