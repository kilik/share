<?php

namespace App\Controller;

use App\Entity\Message;
use App\Manager\MessageManager;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @var MessageManager
     */
    private $messageManager;

    /**
     * DefaultController constructor.
     * @param MessageManager $messageManager
     */
    public function __construct(MessageManager $messageManager)
    {
        $this->messageManager = $messageManager;
    }

    /**
     * @Route("/", name="default")
     *
     * @param Request $request
     *
     * @Template()
     *
     * @return array
     *
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $uuid = Uuid::uuid4();

        return [
            'uuid' => $uuid,
        ];
    }

    /**
     * @Route("/message", name="message_post", methods={"POST"})
     *
     * @param Request $request
     *
     * @Template()
     *
     * @return JsonResponse
     */
    public function message(Request $request)
    {
        try {
            $json = json_decode($request->getContent(), true);

            if (false === $json) {
                throw new \Exception('bad query format');
            }
            $json += [
                'encryptedMessage' => '',
                'encryptedPassPhrase' => '',
                'uuid' => '',
                'maxViews' => 1,
                'delay' => 10,
                'delayType' => 'minute',
            ];

            if ('' === $json['encryptedMessage'] || '' === $json['encryptedPassPhrase'] || '' === $json['uuid']) {
                throw new \Exception('bad query format (missing data)');
            }

            $message = new Message();
            $message->setMessage($json['encryptedMessage']);
            $message->setPassPhrase($json['encryptedPassPhrase']);
            $message->setUuid(Uuid::fromString($json['uuid']));
            $message->setRemainingViews(max(min(50, $json['maxViews']), 1));

            $this->messageManager->saveMessage($message, $json['delay'], $json['delayType']);

            return new JsonResponse([
                'status' => 'ok',
                'url' => $request->getSchemeAndHttpHost() . $this->generateUrl('open', ["uuid" => $message->getUuid()]),
                'expiration' => $message->getExpiredAt()->getTimestamp(),
                'uuid' => (string)$message->getUuid(),
                'maxViews'=>$message->getRemainingViews(),
                'newUuid' => Uuid::uuid4(), // random new uuid

            ]);
        } catch (\Exception $e) {
            return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @Route("/message/{uuid}", name="open", methods={"GET"})
     *
     * @param string $uuid
     *
     * @Template()
     *
     * @Method("GET")
     *
     * @return array
     */
    public function open(string $uuid)
    {
        return [
            'uuid' => $uuid,
        ];
    }

    /**
     * @Route("/message/{uuid}", name="decrypt", methods={"POST"})
     *
     * @param Request $request
     * @param string $uuid
     *
     * @return JsonResponse
     */
    public function decode(Request $request, string $uuid)
    {
        /** @var Message $message */
        $message = $this->getDoctrine()->getRepository(Message::class)->findOneByUuid($uuid);

        $json = json_decode($request->getContent(), true);
        $json += ['encryptedPassPhrase' => ''];

        if (null !== $message) {
            if ($message->getPassPhrase() != $json['encryptedPassPhrase']) {
                $message = null;
            } else {
                // message could be deleted (if expired in the past), so it will be equals to null
                $message = $this->messageManager->readMessage($message);
            }
        } else {
            $message = null;
        }

        // if message is not available (not granted, expired, etc...)
        if (null === $message) {
            return new JsonResponse(['status' => 'error', 'message' => 'bad pass phrase, or this message not exists'], Response::HTTP_NOT_FOUND);
        }

        // else, send message
        return new JsonResponse(['status' => 'success', 'message' => $message->getMessage(), 'remainingViews' => $message->getRemainingViews()]);
    }

}
