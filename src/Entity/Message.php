<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private $uuid;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $message;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $passPhrase;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTime $expiredAt;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\GreaterThanOrEqual(value=1)
     * @Assert\LessThanOrEqual(value=60)
     */
    private int $remainingViews = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?UuidInterface
    {
        return $this->uuid;
    }

    public function setUuid(UuidInterface $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getPassPhrase(): ?string
    {
        return $this->passPhrase;
    }

    public function setPassPhrase(?string $passPhrase): self
    {
        $this->passPhrase = $passPhrase;

        return $this;
    }

    public function getExpiredAt(): ?\DateTimeInterface
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(\DateTimeInterface $expiredAt): self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function getRemainingViews(): ?int
    {
        return $this->remainingViews;
    }

    public function setRemainingViews(int $remainingViews): self
    {
        $this->remainingViews = $remainingViews;

        return $this;
    }

    public function decRemainingViews(): void
    {
        $this->remainingViews--;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist(): void
    {
        if (null === $this->uuid) {
            $this->uuid = Uuid::uuid4();
        }
    }

}
