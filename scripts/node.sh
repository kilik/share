#!/usr/bin/env bash
# simple shortcut to launch node commands with docker

docker run -it --rm --name node-share -v $(pwd):/app -w /app node:14.15-buster $*