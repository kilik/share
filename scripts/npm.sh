#!/usr/bin/env bash
# simple shortcut to launch npm commands with docker
RELATIVE_PATH="$(dirname $0)"
${RELATIVE_PATH}/node.sh npm $*
