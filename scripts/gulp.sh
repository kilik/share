#!/usr/bin/env bash
# simple shortcut to launch node commands with docker

# break on error
set -e

RELATIVE_PATH="$(dirname $0)"

# include common constants and functions
source ${RELATIVE_PATH}/.include.sh

GULP_SCRIPT=node_modules/gulp/bin/gulp.js

if [ ! -f "${GULP_SCRIPT}" ]; then
  echo -e "$COLOR_RED ***************************$COLOR_DEFAULT"
  echo -e "$COLOR_RED !! gulp is not installed !!$COLOR_DEFAULT"
  echo -e "$COLOR_RED ***************************$COLOR_DEFAULT"
  echo -e "You should probably run $COLOR_BLUE./scripts/npm.sh install$COLOR_DEFAULT before"
  exit 1
fi

${RELATIVE_PATH}/node.sh ${GULP_SCRIPT} $*
