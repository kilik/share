.PHONY: help fixtures

# User Id
UID = $(shell id -u)
USER=root

## Display this help text
help:
	$(info ---------------------------------------------------------------------)
	$(info -                        Available targets                          -)
	$(info ---------------------------------------------------------------------)
	@awk '/^[a-zA-Z\-\_0-9]+:/ {                                                \
	nb = sub( /^## /, "", helpMsg );                                            \
	if(nb == 0) {                                                               \
		helpMsg = $$0;                                                      \
		nb = sub( /^[^:]*:.* ## /, "", helpMsg );                           \
	}                                                                           \
	if (nb)                                                                     \
		printf "\033[1;31m%-" width "s\033[0m %s\n", $$1, helpMsg;          \
	}                                                                           \
	{ helpMsg = $$0 }'                                                          \
	$(MAKEFILE_LIST) | column -ts:

.env:
	cp .env.dist .env

## Pull images used in docker-compose config
pull: .env
	docker-compose pull

## Start all containers
up: .env
	docker-compose up -d
## Alias -> up
start: up

## Stop all the containers
stop:
	docker-compose stop

## Stop, then... start
restart: stop start

## Down all the containers (remove all unpersisted data)
down:
	docker-compose down --remove-orphans --volumes

## Show containers running
ps:
	docker-compose ps

## Enter in a php container
php:
	docker-compose exec -u www-data php bash

## Reload fixtures (with migrations)
fixtures:
	docker-compose exec -u www-data php bin/console doctrine:database:drop --force --if-exists
	docker-compose exec -u www-data php bin/console doctrine:database:create
	docker-compose exec -u www-data php bin/console doctrine:migrations:migrate -n
	docker-compose exec -u www-data php bin/console hautelook:fixtures:load -n

## Reload fixtures (from scratch, without migrations)
fixtures_rebuild:
	docker-compose exec -u www-data php bin/console doctrine:database:drop --force --if-exists
	docker-compose exec -u www-data php bin/console doctrine:database:create
	docker-compose exec -u www-data php bin/console doctrine:schema:update --force
	docker-compose exec -u www-data php bin/console hautelook:fixtures:load -n

## Enter in a mysql shell (USER=myuser, default root)
mysql:
	docker-compose exec db mysql -h db -p -u ${USER}

## Update composer + migrations
internal_update:
	docker-compose exec -u www-data php composer install
	docker-compose exec -u www-data php bin/console doctrine:migrations:migrate -n

## Front update
front_update:
	./scripts/npm.sh install
	./scripts/gulp.sh vendor

## Upgrade the stack
upgrade: pull up internal_update front_update

