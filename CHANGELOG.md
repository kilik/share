### 1.2.2
* FIX tooltip / css on copy link

### 1.2.1
* FIX add copy of link only

### 1.2.0
* ADD copy link added on bookmark name
* FIX responsive layout meta data

### 1.1.0
* ADD add form controls
* ADD expiration and max views in bookmark list
* ADD bookmark name in bookmark list
* UPD replace theme by SBADMIN2
* UPD switch to nodejs / npm / gulp to download and build front assets
* UPD update to Symfony 4.4
* FIX notify on message encrypted
* REM vuejs components removed

### 1.0.1
* ADD decrypted messages in textarea (for easy copy&paste)
* UPD fix for php 7.4 compatibility

### 1.0.0
* ADD initial release
