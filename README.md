# KilikShare

This tool is a safe solution to exchange password, tokens, and others confidential content with people.

## to work quickly on this project

requirements:

- a linux based distribution (recommended)
- docker 18+ installed - [install docker](https://docs.docker.com/install/linux)
- a reverse proxy (like [jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy))
- make (recommended)
- git

```bash
git clone git@gitlab.com:kilik/share.git
cd share
make upgrade
```

You should access the following endpoints:

- [local instance](http://share.localhost/) 
- [phpmyadmin](http://pma.share.localhost/)

## How it works

![process](doc/process.png)

- 0 - the sender fill a form with a message and a pass phrase
- 1.1 - the message is encrypted with pass phrase and sent with the pass phrase salted hash
- 1.2 - a resource link is displayed to the sender
- 2 - the sender communicate the resource link and the pass phrase to a receiver (on 2 different medium)
- 3.1 - the receiver open the kilik share link and send the phrase salted hash to the server
- 3.2 - if hash match, encrypted message is returned to the receiver, decryption is done on receiver client side only

Notes:

- step 3.2 is limited to "max views" requests
- the pass phrase is never transmitted to the server
- clear message is never transmitted to the server
- resources identifiers are random UUID (and used to salt encryption and pass phrase hash)
- users can check http transmissions, no confidential / clear data are communicated to/from the server
- expired messages are permanently deleted
- error messages are fuzzy (no difference between bad pass phrase and unknown identifiers)
- a valid pass phrase hash is needed to receive the encoded message (avoid brut force on client side)

## for developers

see scripts/*.sh commands to use nodejs, npm, gulp, etc... shortcuts.

## gitflow / release

* [init git flow](doc/gitflow/init.md)

Create and publish a release:

```shell
read VERSION
git checkout develop
git pull
git flow release start ${VERSION}
./scripts/changelog.sh ${VERSION}
git add .
git commit -m ${VERSION}
git flow release publish
git flow release finish

git push --tags
git push
git checkout master && git push
```